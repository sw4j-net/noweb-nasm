# Introduction

This project creates a docker image with noweb (for literate programming) and nasm installed.

The image can be used to literate program assembly or as a base image for other images.

This repository is mirrored to https://gitlab.com/sw4j-net/noweb-nasm
